// Global variables
console.log("__filename");
console.log(__filename);
console.log("__dirname");
console.log(__dirname);
console.log("require");
console.log(require);
console.log("module");
console.log(module);
console.log("process");
//console.log(process);
/*
output

$ node app.js
__filename
D:\source\NodejsAndExpress\FreeCodeCamp\app.js
__dirname
D:\source\NodejsAndExpress\FreeCodeCamp
require
[Function: require] {
  resolve: [Function: resolve] { paths: [Function: paths] },
  main: Module {
    id: '.',
    path: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp',
    exports: {},
    parent: null,
    filename: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\app.js',
    loaded: false,
    children: [],
    paths: [
      'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\node_modules',
      'D:\\source\\NodejsAndExpress\\node_modules',
      'D:\\source\\node_modules',
      'D:\\node_modules'
    ]
  },
  extensions: [Object: null prototype] {
    '.js': [Function (anonymous)],
    '.json': [Function (anonymous)],
    '.node': [Function (anonymous)]
  },
  cache: [Object: null prototype] {
    'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\app.js': Module {
      id: '.',
      path: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp',
      exports: {},
      parent: null,
      filename: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\app.js',
      loaded: false,
      children: [],
      paths: [Array]
    }
  }
}
module
Module {
  id: '.',
  path: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp',
  exports: {},
  parent: null,
  filename: 'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\app.js',
  loaded: false,
  children: [],
  paths: [
    'D:\\source\\NodejsAndExpress\\FreeCodeCamp\\node_modules',
    'D:\\source\\NodejsAndExpress\\node_modules',
    'D:\\source\\node_modules',
    'D:\\node_modules'
  ]
}
process
*/