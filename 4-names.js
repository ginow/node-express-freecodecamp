// local
const secret = 'Super secret'
// share
const john = 'john'
const peter = 'peter'
//console.log(module);
module.exports = { john, peter }
/*
output:

 node 3-modules.js
names:
{ john: 'john', peter: 'peter' }
function called:
Hello there john
Hello there peter
*/